package com.allan.yocojack.engine;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.allan.yocojack.domain.Card;
import com.allan.yocojack.domain.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GamePlayerImplTest {

	@Autowired
	private GamePlayer gamePlayer;

	@Autowired
	private GameParser gameParser;

	@Test
	public void playHand1() throws Exception {
		List<Card> playerA = Arrays.asList(new Card("8", "D"), new Card("J", "S"));
		List<Card> playerB = Arrays.asList(new Card("5", "D"), new Card("Q", "S"), new Card("3", "H"));
		boolean playerAWins = gamePlayer.playHand(playerA, playerB);
		Assert.assertEquals(false, playerAWins);
	}

	@Test
	public void playHand2() throws Exception {
		List<Card> playerA = Arrays.asList(new Card("10", "D"), new Card("J", "H"));
		List<Card> playerB = Arrays.asList(new Card("Q", "S"), new Card("4", "C"), new Card("J", "C"));
		boolean playerAWins = gamePlayer.playHand(playerA, playerB);
		Assert.assertEquals(true, playerAWins);
	}

	@Test
	public void playHand3() throws Exception {
		List<Card> playerA = Arrays.asList(new Card("J", "H"), new Card("K", "S"), new Card("3", "C"));
		List<Card> playerB = Arrays.asList(new Card("J", "C"), new Card("Q", "S"));
		boolean playerAWins = gamePlayer.playHand(playerA, playerB);
		Assert.assertEquals(false, playerAWins);
	}

	@Test
	public void playHand4() throws Exception {
		List<Card> playerA = Arrays.asList(new Card("3", "C"), new Card("2", "D"), new Card("K", "S"));
		List<Card> playerB = Arrays.asList(new Card("10", "D"), new Card("J", "H"));
		boolean playerAWins = gamePlayer.playHand(playerA, playerB);
		Assert.assertEquals(false, playerAWins);
	}

	@Test
	public void playHand5() throws Exception {
		List<Card> playerA = Arrays.asList(new Card("10", "S"), new Card("8", "C"));
		List<Card> playerB = Arrays.asList(new Card("8", "S"), new Card("10", "H"));
		boolean playerAWins = gamePlayer.playHand(playerA, playerB);
		Assert.assertEquals(true, playerAWins);
	}

	@Test
	public void playHand6() throws Exception {
		List<Card> playerA = Arrays.asList(new Card("9", "D"), new Card("9", "S"));
		List<Card> playerB = Arrays.asList(new Card("9", "C"), new Card("9", "H"));
		boolean playerAWins = gamePlayer.playHand(playerA, playerB);
		Assert.assertEquals(true, playerAWins);
	}

}