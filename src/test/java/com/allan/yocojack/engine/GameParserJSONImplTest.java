package com.allan.yocojack.engine;

import com.allan.yocojack.domain.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameParserJSONImplTest {

	@Autowired
	private GameParser gameParser;

	private String jsonContents = "[\n" +
								  "  {\n" +
								  "    \"playerA\": [\n" +
								  "      \"8D\",\n" +
								  "      \"JS\"\n" +
								  "    ],\n" +
								  "    \"playerAWins\": false,\n" +
								  "    \"playerB\": [\n" +
								  "      \"5D\",\n" +
								  "      \"QS\",\n" +
								  "      \"3H\"\n" +
								  "    ]\n" +
								  "  },\n" +
								  "  {\n" +
								  "    \"playerA\": [\n" +
								  "      \"10D\",\n" +
								  "      \"JH\"\n" +
								  "    ],\n" +
								  "    \"playerAWins\": true,\n" +
								  "    \"playerB\": [\n" +
								  "      \"QS\",\n" +
								  "      \"4C\",\n" +
								  "      \"JC\"\n" +
								  "    ]\n" +
								  "  },\n" +
								  "  {\n" +
								  "    \"playerA\": [\n" +
								  "      \"JH\",\n" +
								  "      \"KS\",\n" +
								  "      \"3C\"\n" +
								  "    ],\n" +
								  "    \"playerAWins\": false,\n" +
								  "    \"playerB\": [\n" +
								  "      \"JC\",\n" +
								  "      \"QS\"\n" +
								  "    ]\n" +
								  "  }\n" +
								  "]";

	@Test
	public void parseGame() throws Exception {
		Game game = gameParser.parseGame(jsonContents);
		Assert.assertEquals(3, game.getRounds().size());
		Assert.assertEquals(false, game.getRounds().get(0).isPlayerAWins());
		Assert.assertEquals(2, game.getRounds().get(0).getPlayerA().size());
		System.out.println();
	}

}