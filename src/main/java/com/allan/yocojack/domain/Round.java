package com.allan.yocojack.domain;

import java.util.List;

public class Round {

	private List<String> playerA;
	private List<String> playerB;
	private boolean playerAWins;

	public List<String> getPlayerA() {
		return playerA;
	}

	public void setPlayerA(List<String> playerA) {
		this.playerA = playerA;
	}

	public List<String> getPlayerB() {
		return playerB;
	}

	public void setPlayerB(List<String> playerB) {
		this.playerB = playerB;
	}

	public boolean isPlayerAWins() {
		return playerAWins;
	}

	public void setPlayerAWins(boolean playerAWins) {
		this.playerAWins = playerAWins;
	}
}
