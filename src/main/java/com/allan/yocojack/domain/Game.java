package com.allan.yocojack.domain;

import java.util.List;

public class Game {

	private List<Round> rounds;

	public Game(List<Round> rounds) {
		this.rounds = rounds;
	}

	public List<Round> getRounds() {
		return rounds;
	}

	public void setRounds(List<Round> rounds) {
		this.rounds = rounds;
	}
}
