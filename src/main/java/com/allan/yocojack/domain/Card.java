package com.allan.yocojack.domain;

public class Card {

	private Face face;
	private Suit suit;

	public Card(String face, String suit) {
		this.face = new Face(face);
		this.suit = new Suit(suit);
	}

	public Face face() {
		return face;
	}

	public Suit suit() {
		return suit;
	}

	public class Face {

		private String face;

		public Face(String face) {
			this.face = face;
		}

		public int getValue() {
			switch (face) {
				case "A": return 11;
				case "K": return 10;
				case "Q": return 10;
				case "J": return 10;
				default: return Integer.valueOf(face);
			}
		}

		public int getRanking() {
			switch (face) {
				case "A": return 14;
				case "K": return 13;
				case "Q": return 12;
				case "J": return 11;
				default: return Integer.valueOf(face);
			}
		}

	}

	public class Suit {

		private String suit;

		public Suit(String suit) {
			this.suit = suit;
		}

		public int getRanking() {
			switch (suit) {
				case "S": return 4;
				case "H": return 3;
				case "C": return 2;
				case "D": return 1;
				default: return 0;
			}
		}
	}

}
