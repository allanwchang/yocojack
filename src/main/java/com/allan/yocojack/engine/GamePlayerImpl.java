package com.allan.yocojack.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.allan.yocojack.domain.Card;
import com.allan.yocojack.domain.Game;
import com.allan.yocojack.domain.Round;

public class GamePlayerImpl implements GamePlayer {

	private static final String CARD_REGEX = "([2-9]|10|J|K|Q|A)(H|D|C|S)";

	@Override
	public void playGame(Game game) {
		List<Round> rounds = game.getRounds();
		int roundCount = 0;
		for (Round round : rounds) {
			roundCount++;
			List<Card> playerA = convertCards(round.getPlayerA());
			List<Card> playerB = convertCards(round.getPlayerB());
			boolean actualResult = playHand(playerA, playerB);
			boolean predictedResult = round.isPlayerAWins();
			boolean match = true;
			if (actualResult != predictedResult) {
				match = false;
			}
			System.out.println(
					String.format("Round: %d | Predicted: %s | Actual: %s | Match: %s", roundCount, predictedResult, actualResult, match));
		}
	}

	private List<Card> convertCards(List<String> cardStrings) {
		Pattern pattern = Pattern.compile(CARD_REGEX, Pattern.MULTILINE);
		List<Card> result = new ArrayList<>();
		for (String cardString : cardStrings) {
			Matcher matcher = pattern.matcher(cardString);
			matcher.find();
			Card card = new Card(matcher.group(1), matcher.group(2));
			result.add(card);
		}
		return result;
	}

	@Override
	public boolean playHand(List<Card> playerA, List<Card> playerB) {
		int playerATotal = evaluateCount(playerA);
		int playerBTotal = evaluateCount(playerB);
		// Check if players go over 21
		if (playerATotal > 21) {
			return false;
		}
		if (playerBTotal > 21) {
			return true;
		}
		if (playerATotal > playerBTotal) {
			return true;
		}
		if (playerATotal < playerBTotal) {
			return false;
		}
		return evaluateTie(playerA, playerB);
	}

	private int evaluateCount(List<Card> cards) {
		int total = 0;
		for (Card card : cards) {
			total = total + card.face().getValue();
		}

		return total;
	}

	private Boolean evaluateTie(List<Card> playerA, List<Card> playerB) {
		Comparator<Card> faceComparator = new Comparator<Card>() {
			@Override
			public int compare(Card card1, Card card2) {
				if (card1.face().getRanking() == card2.face().getRanking()) {
					return Integer.valueOf(card2.suit().getRanking()).compareTo(card1.suit().getRanking());
				} else {
					return Integer.valueOf(card2.face().getRanking()).compareTo(card1.face().getRanking());
				}
			}
		};
		Collections.sort(playerA, faceComparator);
		Collections.sort(playerB, faceComparator);

		int minNumCards = playerA.size() > playerB.size() ? playerB.size() : playerA.size();

		for (int i = 0; i < minNumCards; i++) {
			Card playerACard = playerA.get(i);
			Card playerBCard = playerB.get(i);
			if (playerACard.face().getRanking() > playerBCard.face().getRanking()) {
				return true;
			} else if (playerACard.face().getRanking() < playerBCard.face().getRanking()) {
				return false;
			}
		}

		for (int i = 0; i < minNumCards; i++) {
			Card playerACard = playerA.get(i);
			Card playerBCard = playerB.get(i);
			if (playerACard.suit().getRanking() > playerBCard.suit().getRanking()) {
				return true;
			} else if (playerACard.suit().getRanking() < playerBCard.suit().getRanking()) {
				return false;
			}
		}

		return null;
	}



}
