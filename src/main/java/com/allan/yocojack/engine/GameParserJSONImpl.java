package com.allan.yocojack.engine;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

import com.allan.yocojack.domain.Game;
import com.allan.yocojack.domain.Round;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GameParserJSONImpl implements GameParser {

	@Override
	public Game parseGame(String content) {
		ObjectMapper objectMapper = new ObjectMapper();
		Round[] rounds = null;
		try {
			rounds = objectMapper.readValue(content, Round[].class);
			Game game = new Game(Arrays.asList(rounds));
			return game;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
