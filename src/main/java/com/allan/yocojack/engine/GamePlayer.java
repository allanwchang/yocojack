package com.allan.yocojack.engine;

import java.util.List;

import com.allan.yocojack.domain.Card;
import com.allan.yocojack.domain.Game;

public interface GamePlayer {

	public void playGame(Game game);

	public boolean playHand(List<Card> playerA, List<Card> playerB);
}
