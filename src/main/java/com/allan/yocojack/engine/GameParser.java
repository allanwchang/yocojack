package com.allan.yocojack.engine;

import com.allan.yocojack.domain.Game;

public interface GameParser {

	public Game parseGame(String content);
}
