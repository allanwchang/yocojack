package com.allan.yocojack;

import com.allan.yocojack.engine.GameParser;
import com.allan.yocojack.engine.GameParserJSONImpl;
import com.allan.yocojack.engine.GamePlayer;
import com.allan.yocojack.engine.GamePlayerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

	@Bean
	public GameParser gameParser() {
		return new GameParserJSONImpl();
	}

	@Bean
	public GamePlayer gamePlayer() {
		return new GamePlayerImpl();
	}

}
