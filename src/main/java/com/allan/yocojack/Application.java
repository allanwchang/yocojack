package com.allan.yocojack;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

import com.allan.yocojack.domain.Game;
import com.allan.yocojack.engine.GameParser;
import com.allan.yocojack.engine.GamePlayer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) throws IOException {
		return args -> {
			String content = new Scanner(new URL("https://s3-eu-west-1.amazonaws.com/yoco-testing/tests.json").openStream(),
										 "UTF-8").useDelimiter("\\A").next();
			GameParser gameParser = (GameParser)ctx.getBean("gameParser");
			GamePlayer gamePlayer = (GamePlayer)ctx.getBean("gamePlayer");
			Game game = gameParser.parseGame(content);
			gamePlayer.playGame(game);
		};
	}

}
